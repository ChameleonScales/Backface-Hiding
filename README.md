# Backface-Hiding

This Blender add-on hides the backfacing or non-visible geometry to address the issue of Backface Culling not hiding mesh components in Edit mode (vertices, edges, faces).

It is mostly useful for retopology with X-Ray mode, but isn't limited to this use case.

This image shows a comparison between the built-in Backface culling and the Backface hiding from this add-on when doing retopology on top of a high-poly mesh with X-ray enabled on the low poly mesh (click to enlarge):
 
[![Backface Hiding screenshot](https://i.imgur.com/0u0vUZql.png)](https://i.imgur.com/0u0vUZq.png)

![][1] **Warning :** This add-on doesn't perform in real-time like Backface Culling, it only updates the hidden geometry every time you click on the button or press the keyboard shortcut.

## Installing (regular python addon) :

* Save the raw [*Backface_Hiding_v0_3.py*](https://gitlab.com/ChameleonScales/Backface-Hiding/raw/master/Backface_Hiding_v0_3.py) file anywhere on your computer
* In Blender, open the User Preferences and go in the “Addons” tab
* Click on “Install from file” at the bottom
* Find the addon you downloaded
* Enable it with the checkbox
* You should now find the buttons in the Properties region of any 3DView in Edit mode, under Mesh Display.

## Operators :

* **Hide backfacing :** hides the backfacing geometry (can be revealed with Alt+H or better with Ctrl+Alt+H)
* **Hide non-visible :** hides the geometry that's occluded by other geometry from the current view as well as the geometry outside of the screen area of the 3DView editor.
* **Reveal & restore selection :** reveals the hidden faces and restores the mesh selection you made before hiding.

    ![][1] **Note :** since the selection-restoring is based on indices, if you add or delete mesh elements between the hiding and the revealing, the elements that get re-selected may not be the same as those you originally selected (but obviously your mesh won't be affected).
    
## Keyboard shortcuts :

This add-on also adds default keyboard shortcuts for each operator (Q, D and Ctrl+Alt+H). You can modify or disable them directly from the add-on's preferences.

## TODO list :

Update for Blender 2.8

Feel free to ask for an improvement you'd like to see or give any feedback in the [blenderartists thread](https://blenderartists.org/t/addon-backface-hiding/700822) or through an [issue](https://gitlab.com/ChameleonScales/Backface-Hiding/issues) from here.


This addon was made on Blender 2.79


[1]: https://gitlab.com/ChameleonScales/Blender-individual-icons/raw/master/PNG-16x16/ICON_ERROR.png
