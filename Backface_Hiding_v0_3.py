# ##### BEGIN GPL LICENSE BLOCK #####
#
# Backface Hiding hides backfacing or non-visible geometry in Edit mode.
# Copyright (C) 2018 Caetano Veyssières
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
	"name": "Backface hiding",
	"description": "button to Hide backfacing geometry in Edit mode",
	"author": "Caetano Veyssières (ChameleonScales) with precious help from Secrop and Pistiwique",
	"version": (0, 3),
	"blender": (2, 79, 0),
	"location": "3D View(s) -> Properties Region -> Mesh Display",
	"warning": "This add-on doesn't perform in real-time like Backface Culling, it only updates the hidden geometry every time you press the shortcut or click on the button.",
	"wiki_url": "https://github.com/ChameleonScales/Backface-Hiding",
	"tracker_url": "https://github.com/ChameleonScales/Backface-Hiding/issues",
	"category": "3D View",
	}

import bpy
from bpy.types import AddonPreferences, PropertyGroup
from bpy.props import EnumProperty
import rna_keymap_ui

handle = []
bm_old = [None]

class MemorizeSelection(bpy.types.PropertyGroup):

	# Store original selection :
	select_mode = {}
	vertex_selection = {}
	edge_selection = {}
	face_selection = {}

class MESH_OT_restore_selection(bpy.types.Operator) :
	bl_idname = "mesh.restore_selection"
	bl_label = "Restore Selection"
	bl_description = "Restores the selection of vertices, edges and faces"
	bl_options = {'INTERNAL'}
	def execute(self, context) :
		
		obj = context.object
		meshData = obj.data

		#restore select mode
		bpy.context.tool_settings.mesh_select_mode=obj.memorize_selection.select_mode["selectMode"]
		
		bpy.ops.object.mode_set(mode='OBJECT')
		
		# select mesh components from index dictionaries
		for idx in obj.memorize_selection.vertex_selection["vertex"]:
			meshData.vertices[idx].select = True
		for idx in obj.memorize_selection.edge_selection["edge"]:
			meshData.edges[idx].select = True
		for idx in obj.memorize_selection.face_selection["face"]:
			meshData.polygons[idx].select = True
		
		bpy.ops.object.mode_set(mode='EDIT')
		
		return {'FINISHED'}

class MESH_OT_store_selection(bpy.types.Operator):
	bl_idname = "mesh.store_selection"
	bl_label = "Store Selection"
	bl_description = "memorizes the mesh selection"
	bl_options = {'INTERNAL'}

	@classmethod
	def poll(cls, context):
		return context.object is not None and context.object.data.is_editmode

	def execute(self, context) :
		obj = context.object
		meshData = obj.data
		
		# store select mode
		obj.memorize_selection.select_mode["selectMode"] = bpy.context.tool_settings.mesh_select_mode[0:3]
		
		bpy.ops.object.mode_set(mode='OBJECT')
		
		# store vertex indices in dictionary
		bpy.context.object.memorize_selection.vertex_selection["vertex"] = [v.index for v in meshData.vertices if v.select]
		bpy.context.object.memorize_selection.edge_selection["edge"] = [e.index for e in meshData.edges if e.select]
		bpy.context.object.memorize_selection.face_selection["face"] = [f.index for f in meshData.polygons if f.select]
		
		bpy.ops.object.mode_set(mode='EDIT')
		
		return {'FINISHED'}


class HideBackfacingOperator(bpy.types.Operator):
	bl_idname = "mesh.hide_backfacing"
	bl_label = "Hide Backfacing"
	bl_description = "Hide backfacing geometry"
	bl_options = {'REGISTER','UNDO'}

	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context) :
		bpy.ops.mesh.store_selection()
		
		# Un-hide everything (and go in face selection mode)
		bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='FACE')
		bpy.ops.mesh.reveal()
		
		# Add a plane and rotate it to face the view :
		rotView=bpy.context.region_data.view_matrix.transposed().to_euler()
		bpy.ops.mesh.primitive_plane_add(radius=1,location=(0,0,0),rotation=rotView)
			
		# Make the plane active (for later deletion) :
		bpy.ops.object.mode_set(mode='OBJECT')
		for face in bpy.context.object.data.polygons:
			if face.select:
				bpy.context.object.data.polygons.active = face.index
		
		# Select and hide polygons turning away from created plane
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_similar(type='NORMAL', threshold=0.5)
		bpy.ops.mesh.hide(unselected=True)
		bpy.ops.mesh.select_all(action='DESELECT')
		
		# Select the plane and delete it
		bpy.ops.object.mode_set(mode='OBJECT')
		for face in bpy.context.object.data.polygons:
			if bpy.context.object.data.polygons.active==face.index:
				face.select=True
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.delete(type='FACE')
		
		bpy.ops.mesh.restore_selection()
		
		return {'FINISHED'}

class HideNonVisibleOperator(bpy.types.Operator):
	bl_idname = "mesh.hide_non_visible"
	bl_label = "Hide non-visible"
	bl_description = "Hide non-visible geometry"
	bl_options = {'REGISTER','UNDO'}

	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):
		bpy.ops.mesh.store_selection()
		
		bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='FACE')
		bpy.ops.mesh.reveal()
		
		# Store original "Limit selection to visible" :
		original_occlude_mode = bpy.context.space_data.use_occlude_geometry
		
		# Enable "Limit selection to visible" :
		bpy.context.space_data.use_occlude_geometry = True
		
		# Border select geometry with a rectangle that fills the 3DView Area
		bpy.ops.view3d.select_border(gesture_mode=3, xmin=0, xmax=bpy.context.area.width, ymin=0, ymax=bpy.context.area.height, extend=False)
		
		# Hide non-selected geometry
		bpy.ops.mesh.hide(unselected=True)
		
		bpy.ops.mesh.select_all(action='DESELECT')
		
		# Retore original "Limit selection to visible" :
		bpy.context.space_data.use_occlude_geometry = original_occlude_mode

		bpy.ops.mesh.restore_selection()
		
		return {'FINISHED'}

class UnhideOperator(bpy.types.Operator):
	bl_idname = "mesh.unhide"
	bl_label = "Reveal & restore selection"
	bl_description = "reveals the hidden faces and restores the mesh selection you made before hiding"
	bl_options = {'REGISTER','UNDO'}

	@classmethod
	def poll(cls, context):
		return context.active_object is not None

	def execute(self, context):
		
		bpy.ops.mesh.reveal()
		bpy.ops.mesh.select_all(action='DESELECT')
		bpy.ops.mesh.restore_selection()
		
		return {'FINISHED'}

def displayBackfaceHidingPanel(self, context):

	if context.active_object and context.active_object.type == 'MESH':
		box = self.layout.box()
		box.operator("mesh.hide_backfacing", text="Hide backfacing", icon='VISIBLE_IPO_OFF')
		box.operator("mesh.hide_non_visible", text="Hide non-visible", icon='ORTHO')
		box.operator("mesh.unhide", text="Reveal & restore selection", icon='VISIBLE_IPO_ON')


class AddonPreferences(bpy.types.AddonPreferences):
	bl_idname = __name__
	category = "Properties"
	
	# Hotkeys layout :
	def draw(self, context):
		layout = self.layout
		box = layout.box()
		split = box.split()
		col = split.column()
		col.label('Setup backface hiding Hotkeys :')
		col.separator()
		col.label('Setup Hotkeys')
		col.separator()
		wm = bpy.context.window_manager
		kc = wm.keyconfigs.user
		km = kc.keymaps['Mesh']
		
		kmi = get_hotkey_entry_item(km, 'mesh.hide_backfacing')
		if kmi:
			col.context_pointer_set("keymap", km)
			rna_keymap_ui.draw_kmi([], kc, km, kmi, col, 0)
		else:
			col.label("No hotkey entry found")
			col.operator(BackfaceHidingAddHotkey.bl_idname, text = "Add hotkey entry", icon = 'ZOOMIN')
		
		kmi = get_hotkey_entry_item(km, 'mesh.hide_non_visible')
		if kmi:
			col.context_pointer_set("keymap", km)
			rna_keymap_ui.draw_kmi([], kc, km, kmi, col, 0)
		else:
			col.label("No hotkey entry found")
			col.operator(BackfaceHidingAddHotkey.bl_idname, text = "Add hotkey entry", icon = 'ZOOMIN')
			
		kmi = get_hotkey_entry_item(km, 'mesh.unhide')
		if kmi:
			col.context_pointer_set("keymap", km)
			rna_keymap_ui.draw_kmi([], kc, km, kmi, col, 0)
		else:
			col.label("No hotkey entry found")
			col.operator(BackfaceHidingAddHotkey.bl_idname, text = "Add hotkey entry", icon = 'ZOOMIN')
			
			
		row = box.row(align=True)
		row.label("Save preferences to apply changes in these settings", icon='ERROR')
	
addon_keymaps = [] 

def get_hotkey_entry_item(km, kmi_name):
	'''
	returns hotkey of specific type, with specific properties.name (keymap is not a dict, so referencing by keys is not enough
	if there are multiple hotkeys!)
	'''
	for i, km_item in enumerate(km.keymap_items):
		if km.keymap_items.keys()[i] == kmi_name:
			return km_item
	return None

def add_hotkeys():
	user_preferences = bpy.context.user_preferences
	addon_prefs = user_preferences.addons[__name__].preferences
	
	wm = bpy.context.window_manager
	kc = wm.keyconfigs.addon
	km = kc.keymaps.new(name="Mesh", space_type='EMPTY')
	kmi = km.keymap_items.new("mesh.hide_backfacing",'Q', 'PRESS')
	kmi.active = True
	addon_keymaps.append((km, kmi))
	kmi = km.keymap_items.new("mesh.hide_non_visible",'D', 'PRESS')
	kmi.active = True
	addon_keymaps.append((km, kmi))
	kmi = km.keymap_items.new("mesh.unhide",'H', 'PRESS', alt=True, ctrl=True)
	kmi.active = True
	addon_keymaps.append((km, kmi))

class BackfaceHidingAddHotkeys(bpy.types.Operator):
	''' Add hotkey entries '''
	bl_idname = "backfacehiding.add_hotkeys"
	bl_label = "Addon Preferences Example"
	bl_options = {'REGISTER', 'INTERNAL'}

	def execute(self, context):
		add_hotkeys()

		self.report({'INFO'}, "Hotkeys added in User Preferences -> Input -> 3DView -> Mesh")
		return {'FINISHED'}
    
    
def remove_hotkeys():
	''' clears all addon level keymap hotkeys stored in addon_keymaps '''
	wm = bpy.context.window_manager
	kc = wm.keyconfigs.addon
	km = kc.keymaps['Mesh']
	
	for km, kmi in addon_keymaps:
		km.keymap_items.remove(kmi)
	addon_keymaps.clear()

def register():
	bpy.utils.register_module(__name__)
	bpy.types.VIEW3D_PT_view3d_meshdisplay.append(displayBackfaceHidingPanel)
	bpy.types.Object.memorize_selection = bpy.props.PointerProperty(type=MemorizeSelection)

	# hotkeys setup
	add_hotkeys()
	

def unregister():
	# hotkeys cleanup
	remove_hotkeys()
	
	bpy.utils.unregister_module(__name__)
	bpy.types.VIEW3D_PT_view3d_meshdisplay.remove(displayBackfaceHidingPanel)
	

if __name__ == "__main__":
	register()
